package com.team7.spacebattle;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.team7.spacebattle.screens.MainGameScreen;
import com.team7.spacebattle.tools.BackgroundManagement;


public class SpaceBattle extends Game {

    public SpriteBatch batch;
    private BitmapFont font;

    public void create() {
        batch = new SpriteBatch();

        font = new BitmapFont();
        this.setScreen(new MainGameScreen(this));
        BackgroundManagement backgroundManagement = new BackgroundManagement();
    }

    public void render() {
        super.render();
    }

    public void dispose() {
        batch.dispose();
        font.dispose();
    }

}


