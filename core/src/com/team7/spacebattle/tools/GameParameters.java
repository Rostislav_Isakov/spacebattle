package com.team7.spacebattle.tools;

public interface GameParameters {
    int WIDTH = 1024;
    int HEIGHT = 768;
    float MIN_ASTEROID_SPAWN_TIME = 2f;
    float MAX_ASTEROID_SPAWN_TIME = 3f;
    float MIN_ENEMY_SPAWN_TIME = 1f;
    float MAX_ENEMY_SPAWN_TIME = 2f;
    float MIN_MONEY_SPAWN_TIME = 4f;
    float MAX_MONEY_SPAWN_TIME = 8f;
    float MIN_HEART_SPAWN_TIME = 20f;
    float MAX_HEART_SPAWN_TIME = 40f;

}
