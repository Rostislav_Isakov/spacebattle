package com.team7.spacebattle.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.team7.spacebattle.screens.MainGameScreen;

import java.util.Random;


public class BackgroundManagement {

    private static final int DEFAULT_SPEED = 200;
    private static final int ACCELERATION = 50;
    private static final int GOAL_REACH_ACCELERATION = 200;

    private static Texture image;
    private static float y1;
    private static float y2;
    private static int speed;
    private static int goalSpeed;
    private static float imageScale;
    private static boolean speedFixed;

    public BackgroundManagement () {
        image = new Texture("space_fon.png");
        y1 = 0;
        y2 = image.getHeight();
        speed = 0;
        goalSpeed = DEFAULT_SPEED;
        imageScale = MainGameScreen.WIDTH / image.getWidth();
        speedFixed = true;
    }

    public static void updateAndRender(float deltaTime, SpriteBatch batch) {
        if (speed < goalSpeed) {
            speed += GOAL_REACH_ACCELERATION * deltaTime;
            if (speed > goalSpeed)
                speed = goalSpeed;
        } else if (speed > goalSpeed) {
            speed -= GOAL_REACH_ACCELERATION * deltaTime;
            if (speed < goalSpeed)
                speed = goalSpeed;
        }

        if (!speedFixed)
            speed += ACCELERATION * deltaTime;

        y1 -= speed * deltaTime;
        y2 -= speed * deltaTime;



        if (y1 + image.getHeight() * imageScale <= 0)
            y1 = y2 + image.getHeight() * imageScale;

        if (y2 + image.getHeight() * imageScale <= 0)
            y2 = y1 + image.getHeight() * imageScale;

        batch.draw(image, 0, y1, MainGameScreen.WIDTH, image.getHeight() * imageScale);
        batch.draw(image, 0, y2, MainGameScreen.WIDTH, image.getHeight() * imageScale);
    }

}


