package com.team7.spacebattle.entities;

import com.badlogic.gdx.graphics.Texture;
import com.team7.spacebattle.tools.GameParameters;

public class Heart extends Entity implements GameParameters {
    private static final int SPEED = 150;
    public static final int WIDTH = 30;
    private static final int HEIGHT = 30;

    public Heart(float x) {
        super(x, GameParameters.HEIGHT, SPEED, new Texture("heart.png"), WIDTH, HEIGHT);
    }
}
