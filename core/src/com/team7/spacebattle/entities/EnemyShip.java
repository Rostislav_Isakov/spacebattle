package com.team7.spacebattle.entities;

import com.badlogic.gdx.graphics.Texture;
import com.team7.spacebattle.tools.GameParameters;

public class EnemyShip extends Entity implements GameParameters {
    private static final int SPEED = 250;
    public static final int WIDTH = 64;
    private static final int HEIGHT = 64;

    public EnemyShip(float x) {
        super(x, GameParameters.HEIGHT, SPEED, new Texture("enemy.png"), WIDTH, HEIGHT);
    }

}
