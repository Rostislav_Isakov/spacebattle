package com.team7.spacebattle.entities;

import com.badlogic.gdx.graphics.Texture;
import com.team7.spacebattle.tools.GameParameters;

public class Bullet extends Entity implements GameParameters {
    private static final int SPEED = 350;
    private static final int WIDTH = 20;
    private static final int HEIGHT = 36;
    private float x, y;

    public Bullet(float x, float y) {
        super(x, y, SPEED, new Texture("bullet.png"), WIDTH, HEIGHT);
        this.x = x;
        this.y = y;
}

    public void update(float delta) {
        y += SPEED * delta;
        setY(y);
        getCollision().move(x, y);
    }
}
