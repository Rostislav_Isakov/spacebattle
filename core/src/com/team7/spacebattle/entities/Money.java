package com.team7.spacebattle.entities;

import com.badlogic.gdx.graphics.Texture;
import com.team7.spacebattle.tools.GameParameters;

public class Money extends Entity implements GameParameters {

    private static final int SPEED = 150;
    public static final int WIDTH = 20;
    private static final int HEIGHT = 20;

    public Money(float x) {
        super(x, GameParameters.HEIGHT, SPEED, new Texture("money.png"), WIDTH, HEIGHT);
    }
}
