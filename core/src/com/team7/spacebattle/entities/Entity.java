package com.team7.spacebattle.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.team7.spacebattle.tools.CollisionRect;
import com.team7.spacebattle.tools.GameParameters;

public abstract class Entity implements GameParameters {
    private int speedOfFall;
    private int widthOfTexture;
    private int heightOfTexture;
    private Texture texture;
    private CollisionRect rect;
    private float x, y;
    private boolean remove;


    Entity(float x, float y, int speedOfFall, Texture texture, int widthOfTexture, int heightOfTexture) {
        this.x = x;
        this.y = y;
        this.speedOfFall = speedOfFall;
        this.texture = texture;
        this.widthOfTexture = widthOfTexture;
        this.heightOfTexture = heightOfTexture;
        this.rect = new CollisionRect(x, y, texture.getWidth(), texture.getHeight());
    }

    public void update(float deltaTime) {
        y -= speedOfFall * deltaTime;
        rect.move(x, y);
        if (y<0) remove = true;
    }

    public void render(SpriteBatch batch) {
        batch.draw(texture, x, y, widthOfTexture, heightOfTexture);
    }

    public CollisionRect getCollision() {
        return rect;
    }


    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }


    public boolean isRemove() {
        return remove;
    }

}

