package com.team7.spacebattle.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.team7.spacebattle.SpaceBattle;
import com.team7.spacebattle.tools.GameParameters;

public class GameOverScreen implements GameParameters, Screen {
    private final SpaceBattle game;
    private Texture gameOver;
    private OrthographicCamera camera;

    GameOverScreen(final SpaceBattle gam) {
        game = gam;
        gameOver = new Texture(Gdx.files.internal("game_over.png"));

        camera = new OrthographicCamera();
        camera.setToOrtho(false, WIDTH, HEIGHT);
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        //Рисуем фон
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // сообщает камере, что нужно обновить матрицы.

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        // начинаем новую серию, рисуем шрифты
        game.batch.begin();
        game.batch.draw(gameOver, 0, 0, WIDTH, HEIGHT);
        game.batch.end();

        //Отслеживание касания пользователя

        if (Gdx.input.isTouched()) {
            game.setScreen(new MainGameScreen(game));
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }


}
