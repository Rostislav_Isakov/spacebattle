package com.team7.spacebattle.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.team7.spacebattle.SpaceBattle;
import com.team7.spacebattle.animation.AnimationFirstShip;
import com.team7.spacebattle.entities.Asteroid;
import com.team7.spacebattle.entities.Bullet;
import com.team7.spacebattle.entities.EnemyShip;
import com.team7.spacebattle.entities.Entity;
import com.team7.spacebattle.entities.Heart;
import com.team7.spacebattle.entities.Money;
import com.team7.spacebattle.tools.BackgroundManagement;
import com.team7.spacebattle.tools.CollisionRect;
import com.team7.spacebattle.tools.GameParameters;

import java.util.ArrayList;
import java.util.Random;

public class MainGameScreen implements GameParameters, Screen {
    private final SpaceBattle game;
    private final AnimationFirstShip mainShipAnimation;

    private TextureRegion currentFrames;

    private Sound collisionSound;
    private Music BackgroundMusic;

    private SpriteBatch batch;
    private OrthographicCamera camera;

    private Rectangle mainShip;

    private ArrayList<EnemyShip> shipsOfEnemy;
    private ArrayList<Asteroid> asteroids;
    private ArrayList<Bullet> bullets;
    private ArrayList<Money> money;
    private ArrayList<Heart> hearts;

    private ArrayList<Money> moneyToRemove = new ArrayList<>();
    private ArrayList<Asteroid> asteroidsToRemove = new ArrayList<>();
    private ArrayList<Bullet> bulletsToRemove = new ArrayList<>();
    private ArrayList<Heart> heartsToRemove = new ArrayList<>();
    private ArrayList<EnemyShip> enemyShipsToRemove = new ArrayList<>();

    private Random random;

    private float enemySpawnTimer;
    private float asteroidSpawnTimer;
    private float bulletSpawnTimer;
    private float moneySpawnTimer;
    private float heartsSpawnTimer;

    private BitmapFont font;

    private long qualityOfMoney;
    private long qualityOfHearts;
    private static double score;




    private CollisionRect playerRect;

    public MainGameScreen(final SpaceBattle gam) {
        this.game = gam;

        Texture texture = new Texture("main_ship_anim.png");
        mainShipAnimation = new AnimationFirstShip(new TextureRegion(texture), 8, 0.5f);

        font = new BitmapFont();

        random = new Random();

        collisionSound = Gdx.audio.newSound(Gdx.files.internal("collision.mp3"));
        BackgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("area.mp3"));

        BackgroundMusic.setLooping(true);
        BackgroundMusic.setVolume((float) 0.03);
        BackgroundMusic.play();


        camera = new OrthographicCamera();
        camera.setToOrtho(false, WIDTH, HEIGHT);
        batch = new SpriteBatch();

        mainShip = new Rectangle();
        mainShip.x = WIDTH / 2 - 64 / 2;
        mainShip.y = 20;
        mainShip.width = 64;
        mainShip.height = 64;
        playerRect = new CollisionRect(mainShip.x, mainShip.y, (int) mainShip.width, (int) mainShip.height);
        shipsOfEnemy = new ArrayList<>();
        asteroids = new ArrayList<>();
        bullets = new ArrayList<>();
        money = new ArrayList<>();
        hearts = new ArrayList<>();


    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        BackgroundManagement.updateAndRender(delta, batch);

        mainShipAnimation.update(delta);

        currentFrames= mainShipAnimation.getFrame();

        batch.draw(currentFrames, mainShip.x, mainShip.y, mainShip.width, mainShip.height);



        font.draw(batch, "Money: " + qualityOfMoney, 0, HEIGHT);
        font.draw(batch, "x" + qualityOfHearts, WIDTH / 2, HEIGHT);
        font.draw(batch, "score" + Math.round(score), WIDTH - (WIDTH / 6), HEIGHT);
        for (EnemyShip enemyShip : shipsOfEnemy) {
            enemyShip.render(batch);
        }
        for (Asteroid asteroid : asteroids) {
            asteroid.render(batch);
        }
        for (Bullet bullet : bullets) {
            bullet.render(batch);
        }
        for (Money money : money) {
            money.render(batch);
        }
        for (Heart heart : hearts) {
            heart.render(batch);
        }
        batch.end();

        camera.update();


        batch.setProjectionMatrix(camera.combined);

        score += 0.1;


        if (Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            int speedOfShip = 20;
            if (touchPos.x < WIDTH / 2) {
                mainShip.x -= speedOfShip;
            } else {
                mainShip.x += speedOfShip;
            }
        }
        playerRect.move(mainShip.x, mainShip.y);
        if (Gdx.input.isKeyPressed(Keys.LEFT)) mainShip.x -= 200 * Gdx.graphics.getDeltaTime();
        if (Gdx.input.isKeyPressed(Keys.RIGHT)) mainShip.x += 200 * Gdx.graphics.getDeltaTime();

        if (mainShip.x < 0) mainShip.x = 0;
        if (mainShip.x > WIDTH - mainShip.width) mainShip.x = WIDTH - mainShip.width;

        enemySpawnTimer -= delta;
        if (enemySpawnTimer <= 0) {
            enemySpawnTimer = random.nextFloat() * (MAX_ENEMY_SPAWN_TIME - MIN_ENEMY_SPAWN_TIME) + MIN_ENEMY_SPAWN_TIME;
            shipsOfEnemy.add(new EnemyShip(random.nextInt(WIDTH - EnemyShip.WIDTH)));
        }
        asteroidSpawnTimer -= delta;
        if (asteroidSpawnTimer <= 0) {
            asteroidSpawnTimer = random.nextFloat() * (MAX_ASTEROID_SPAWN_TIME - MIN_ASTEROID_SPAWN_TIME) + MIN_ASTEROID_SPAWN_TIME;
            asteroids.add(new Asteroid(random.nextInt(WIDTH - Asteroid.WIDTH)));
        }
        bulletSpawnTimer -= delta;
        if (bulletSpawnTimer <= 0) {
            bulletSpawnTimer = 0.3f;
            bullets.add(new Bullet(mainShip.x + 23, mainShip.y + mainShip.height));
        }
        moneySpawnTimer -= delta;
        if (moneySpawnTimer <= 0) {
            moneySpawnTimer = random.nextFloat() * (MAX_MONEY_SPAWN_TIME - MIN_MONEY_SPAWN_TIME) + MIN_MONEY_SPAWN_TIME;
            money.add(new Money(random.nextInt(WIDTH - Money.WIDTH)));
        }

        heartsSpawnTimer -= delta;
        if (heartsSpawnTimer <= 0 && qualityOfHearts < 3) {
            heartsSpawnTimer = random.nextFloat() * (MAX_HEART_SPAWN_TIME - MIN_HEART_SPAWN_TIME) + MIN_HEART_SPAWN_TIME;
            hearts.add(new Heart(random.nextInt(WIDTH - Heart.WIDTH)));
        }

        for (EnemyShip ship : shipsOfEnemy) {
            ship.update(delta);
            if (ship.isRemove()) {
                enemyShipsToRemove.add(ship);
            }
        }
        for (Heart heart : hearts) {
            heart.update(delta);
        }

        for (Asteroid asteroid : asteroids) {
            asteroid.update(delta);
            if (asteroid.isRemove()) {
                asteroidsToRemove.add(asteroid);
            }
        }
        for (Money money : money) {
            money.update(delta);
        }
        for (Bullet bullet : bullets) {
            bullet.update(delta);
        }
        checkForCollisionBullet();
        checkForCollisionOfMainShip();

    }


    @Override
    public void dispose() {
// высвобождение всех нативных ресурсов
        collisionSound.dispose();
        BackgroundMusic.dispose();
        batch.dispose();
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {

    }

    private void checkForCollisionBullet() {
        for (Bullet bullet : bullets) {
            for (EnemyShip ship : shipsOfEnemy) {
                if (bullet.getCollision().collidesWith(ship.getCollision())) {
                    bulletsToRemove.add(bullet);
                    enemyShipsToRemove.add(ship);
                }
                for (Asteroid asteroid : asteroids) {
                    if (bullet.getCollision().collidesWith(asteroid.getCollision())) {
                        bulletsToRemove.add(bullet);
                    }
                }
            }
        }
        shipsOfEnemy.removeAll(enemyShipsToRemove);
        bullets.removeAll(bulletsToRemove);

    }


    private void checkForCollisionOfMainShip() {

        for (Money money : money) {
            if (money.getCollision().collidesWith(playerRect)) {
                qualityOfMoney += 10;
                moneyToRemove.add(money);
            }
        }
        money.removeAll(moneyToRemove);
        for (Heart heart : hearts) {
            if (heart.getCollision().collidesWith(playerRect)) {
                qualityOfHearts += 1;
                heartsToRemove.add(heart);
            }
        }
        hearts.removeAll(heartsToRemove);

        for (EnemyShip ship : shipsOfEnemy) {
            if (ship.getCollision().collidesWith(playerRect)) {
                if (qualityOfHearts < 1) {
                    game.setScreen(new GameOverScreen(game));
                    collisionSound.play();
                    BackgroundMusic.stop();
                    score = 0;
                    qualityOfHearts = 0;
                    qualityOfMoney = 0;
                } else qualityOfHearts -= 1;
                enemyShipsToRemove.add(ship);
            }
        }
        shipsOfEnemy.removeAll(enemyShipsToRemove);

        for (Asteroid asteroid : asteroids) {
            if (asteroid.getCollision().collidesWith(playerRect)) {
                if (qualityOfHearts < 1) {
                    game.setScreen(new GameOverScreen(game));
                    collisionSound.play();
                    BackgroundMusic.stop();
                    score = 0;
                    qualityOfHearts = 0;
                    qualityOfMoney = 0;
                } else qualityOfHearts -= 1;
                asteroidsToRemove.add(asteroid);
            }
        }
        asteroids.removeAll(asteroidsToRemove);
    }
}
