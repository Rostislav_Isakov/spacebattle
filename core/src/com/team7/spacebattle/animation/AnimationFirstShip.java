package com.team7.spacebattle.animation;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 *
 * @author Isakov.R
 */

public class AnimationFirstShip {
    private Array<TextureRegion> frames; // массив с кадрами
    private float maxFrameTime;//длительность отображения одного кадра
    private float currentFrameTime;//время отображения текущего кадра
    private int frameCount;//кол во кадров  анимации
    private int frame;//отдельный кадр анимации

    public AnimationFirstShip(TextureRegion region, int frameCount, float cycleTime){//регион текстур кол во кадров аним, длительность цикла анимации
        frames = new Array<TextureRegion>();//инцализ массив текстур
        int frameWidth = region.getRegionWidth() / frameCount;// ширина кадра = ширинка региона текстур / кол во кадров
        for (int i = 0; i < frameCount; i++){// перебираем картинки, поочереди используя их для кадров анимации
            frames.add(new TextureRegion(region, i * frameWidth, 0, frameWidth, region.getRegionHeight()));//
        }
        this.frameCount = frameCount;//
        maxFrameTime = cycleTime / frameCount;//
        frame = 0;//
    }
    public void update(float dt){// если длительность текущ кадра больше максимальной увелич номер кадра пока число кадров не ддостиг установ и так по кругу
        currentFrameTime += dt;//
        if (currentFrameTime > maxFrameTime){//
            frame++;//
            currentFrameTime = 0;//
        }
        if (frame >= frameCount)//
            frame = 0;//
    }
    public  TextureRegion getFrame(){//получ текущего кадра анимации
        return frames.get(frame);//
    }
}